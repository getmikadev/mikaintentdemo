package id.mikaapp.mikaintentdemo.ui.main

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import com.google.android.material.textfield.TextInputEditText
import id.mikaapp.mikaintentdemo.R

class MainFragment : Fragment() {

    companion object {
        const val MIKA_INTENT_CHARGE_STRING = "id.mikaapp.mika.charge"
        const val MIKA_INTENT_PRINT_STRING = "id.mikaapp.mika.print"
        const val MIKA_INTENT_VOID_STRING = "id.mikaapp.mika.void"
        const val MIKA_INTENT_SETTLE_STRING = "id.mikaapp.mika.settlement"
        const val MIKA_INTENT_KEY_APPROVAL_CODE = "APPROVAL_CODE"
        const val MIKA_INTENT_KEY_TRANSACTION_ID = "TRANSACTION_ID"
        const val MIKA_INTENT_REQUEST_CREATE_TRANS_CODE = 10
        const val MIKA_INTENT_REQUEST_PRINT_CODE = 11
        const val MIKA_INTENT_REQUEST_VOID_CODE = 12
        const val MIKA_INTENT_REQUEST_SETTLE_CODE = 13
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private var transidfield: MaterialAutoCompleteTextView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        val createTransBtn = view.findViewById<MaterialButton>(R.id.createtransbtn)
        val printbtn = view.findViewById<MaterialButton>(R.id.printbtn)
        val voidbtn = view.findViewById<MaterialButton>(R.id.voidbtn)
        val settlebtn = view.findViewById<MaterialButton>(R.id.settlebtn)
        val usernamefield = view.findViewById<TextInputEditText>(R.id.form_usernamefield)
        val passwordfield = view.findViewById<TextInputEditText>(R.id.form_passfield)
        val amountfield = view.findViewById<TextInputEditText>(R.id.form_amountfield)
        val acquireridfield = view.findViewById<TextInputEditText>(R.id.form_acquireridfield)
        transidfield = view.findViewById(R.id.form_trxidfield)

        createTransBtn.setOnClickListener {
            try {
                val intent = Intent(MIKA_INTENT_CHARGE_STRING).apply {
                    putExtra("USERNAME", usernamefield.text.toString())
                    putExtra("PASSWORD", passwordfield.text.toString())
                    putExtra("AMOUNT", amountfield.text.toString().toIntOrNull() ?: 0)
                    if (!acquireridfield.text.isNullOrBlank()) {
                        putExtra("ACQUIRER_ID", acquireridfield.text.toString())
                    }
                }
                startActivityForResult(intent, MIKA_INTENT_REQUEST_CREATE_TRANS_CODE)
            } catch (e: Exception) {
                Toast.makeText(requireContext(), e.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }

        printbtn.setOnClickListener {
            try {
                val intent = Intent(MIKA_INTENT_PRINT_STRING).apply {
                    putExtra("USERNAME", usernamefield.text.toString())
                    putExtra("PASSWORD", passwordfield.text.toString())
                    putExtra("TRANSACTION_ID", transidfield!!.text.toString())
                }
                startActivityForResult(intent, MIKA_INTENT_REQUEST_PRINT_CODE)
            } catch (e: Exception) {
                Toast.makeText(requireContext(), e.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }

        voidbtn.setOnClickListener {
            try {
                val intent = Intent(MIKA_INTENT_VOID_STRING).apply {
                    putExtra("USERNAME", usernamefield.text.toString())
                    putExtra("PASSWORD", passwordfield.text.toString())
                    putExtra("TRANSACTION_ID", transidfield!!.text.toString())
                }
                startActivityForResult(intent, MIKA_INTENT_REQUEST_VOID_CODE)
            } catch (e: Exception) {
                Toast.makeText(requireContext(), e.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }

        settlebtn.setOnClickListener {
            try {
                val intent = Intent(MIKA_INTENT_SETTLE_STRING).apply {
                    putExtra("USERNAME", usernamefield.text.toString())
                    putExtra("PASSWORD", passwordfield.text.toString())
                }
                startActivityForResult(intent, MIKA_INTENT_REQUEST_SETTLE_CODE)
            } catch (e: Exception) {
                Toast.makeText(requireContext(), e.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MIKA_INTENT_REQUEST_CREATE_TRANS_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                requireNotNull(data)
                val approvalCode = data.getStringExtra(MIKA_INTENT_KEY_APPROVAL_CODE)
                val transactionId = data.getStringExtra(MIKA_INTENT_KEY_TRANSACTION_ID)
                transidfield?.setText(transactionId)
                Toast.makeText(
                    requireContext(),
                    "TRANSACTION SUCCESS\n" +
                            "Approval Code: $approvalCode\n",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (resultCode == Activity.RESULT_CANCELED) {
                val errorMessage = data?.getStringExtra("ERROR_MESSAGE")
                Toast.makeText(
                    requireContext(),
                    "TRANSACTION CANCELLED\n $errorMessage",
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else if (requestCode == MIKA_INTENT_REQUEST_PRINT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                requireNotNull(data)
            } else if (resultCode == Activity.RESULT_CANCELED) {
                val errorCode = data?.getStringExtra("ERROR_CODE")
                val errorMessage = data?.getStringExtra("ERROR_MESSAGE")
                Toast.makeText(
                    requireContext(),
                    "TRANSACTION FAILED\n $errorCode: $errorMessage",
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else if (requestCode == MIKA_INTENT_REQUEST_VOID_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                requireNotNull(data)
                val approvalCode = data.getStringExtra("APPROVAL_CODE")
                Toast.makeText(
                    requireContext(),
                    "VOID SUCCESS\n" +
                            "Approval Code: $approvalCode",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (resultCode == Activity.RESULT_CANCELED) {
                val errorCode = data?.getStringExtra("ERROR_CODE")
                val errorMessage = data?.getStringExtra("ERROR_MESSAGE")
                Toast.makeText(
                    requireContext(),
                    "TRANSACTION FAILED\n $errorCode: $errorMessage",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}